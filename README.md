# Blog Resources

Resources for my blog, such as diagrams or files. There is an included `Makefile`
containing build scripts. To generate everything, run

    make

To package everything, which by default goes into the `public` folder but the
folder name can be adjusted by changing `DESTDIR`, run

    make install

## LaTeX

I like using LaTeX for diagrams, libraries like TikZ make it very powerful. The
code in `latex/` contains LaTeX sources for diagrams on my blog.
