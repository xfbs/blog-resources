LATEX_SOURCES = $(wildcard latex/*.tex)
LATEX_TARGETS = $(LATEX_SOURCES:%.tex=%.pdf) $(LATEX_SOURCES:%.tex=%.png) $(LATEX_SOURCES:%.tex=%.svg)

TARGETS = ${LATEX_TARGETS}

PNG_DPI = 300

DESTDIR = public

default: ${TARGETS}

clean:
	${RM} ${TARGETS}

# install the resulting files
install: ${TARGETS:%=${DESTDIR}/%}

# copy a file to the destdir
${DESTDIR}/%: %
	mkdir -p $(dir $@)
	cp $< $@

# compile a latex file to pdf
latex/%.pdf: latex/%.tex
	cd latex && latexmk -lualatex "../$<"

# convert pdf file to png
%.png: %.pdf
	pdftoppm "$<" "${@:%.png=%}" -png -singlefile -r "${PNG_DPI}"

# convert pdf file to svg
%.svg: %.pdf
	pdftocairo "$<" "$@" -svg
